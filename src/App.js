import Order from "./components/Order";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from "react";
import { Container } from "reactstrap";
import { Route, Router, Routes } from "react-router-dom";
import ProductDetail from "./components/ProductDetail";
function App() {
  const [totalPrice, setTotalPrice] = useState(0)
  const addPrice = (price) => { setTotalPrice(totalPrice + price) }
  const data_array = [
    { name: "Iphone X", price: 900, quantity: 0, id:"iphone" },
    { name: "Samsung S9", price: 800, quantity: 2,id:"samsung" },
    { name: "Nokia 8", price: 650, quantity: 0, id:"nokia" }
]
  return (
    <Container>
      <h2 >Complex Example (Event,List,Prop,State)</h2>
      <Routes>
        <Route path="/" element={
          <>
            <Order handleBuy={addPrice} data ={data_array}/>
            <label> Total: {totalPrice} $</label>
          </>
        }></Route>
        <Route path="/iphone" element={<ProductDetail product={data_array[0]}></ProductDetail>}></Route>
        <Route path="/samsung" element={<ProductDetail product={data_array[1]}></ProductDetail>}></Route>
        <Route path="/nokia" element={<ProductDetail product={data_array[2]}></ProductDetail>}></Route>
      </Routes>

    </Container>
  );
}

export default App;
