import { Grid } from "@mui/material";
import OrderDetail from "./orderContent/OrderDetail";

export default function Order({handleBuy,data}) {
    
    const handleClick = (name, price, quantity) => {
        console.log("name : " + name + " price : " + price + " quantity : " + quantity)
        handleBuy(price)
    }
    return (
        <Grid container>
            {data.map((product,index) => 
                <Grid key={index} item xs={4}>
                    <OrderDetail
                        data={product}
                        handleBuy={handleClick}
                    ></OrderDetail>
                </Grid>
            
            )
            }
        </Grid>
    )
}