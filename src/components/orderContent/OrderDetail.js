import { Button, Stack } from "@mui/material";

export default function OrderDetail ({data,handleBuy}){
    return (
        <div className="border m-2 p-2">
            <h1>{data.name}</h1>
            <h4>Price : {data.price} $</h4>
            <h4>quantity : {data.quantity}</h4>
            <Stack spacing={2} direction="row">
            <Button variant="contained" onClick={()=>handleBuy(data.name,data.price,data.quantity)}>Buy</Button>
            <Button variant="contained" href={"/"+data.id}>Detail</Button>
            </Stack>
        </div>
    )
}