import { Button } from "@mui/material"

export default function ProductDetail({product}){
    console.log(product)
    return (
        <div className="border m-2 p-2">
            <h1>{product.name}</h1>
            <h4>Price : {product.price} $</h4>
            <h4>quantity : {product.quantity}</h4>
            <Button variant="contained" href="/">Back</Button>
        </div>
    )
}